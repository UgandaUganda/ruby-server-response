#### HOW ?  ##############
# read address from stdin
# do cURL on the address (...and subpages??)
# return success code
# sort urls based on success code
# ########################

require 'curb'  
require 'socket'

# input stdin
puts "Domain: "
domain = gets
domain.chomp!

# protocol switches
# should be constants
	http = false
	ssl = false
	wtf = false

# input validation
if domain.start_with? "http://" 
	puts " = HTTP protocol"
	http = true
	  spl = domain.split("http://")
		spl = spl[1]
	  if spl.end_with? ("/") 
		spl = spl.split("/")
		spl = spl[0]
		puts "Domain name: " + spl
	  end
	serverip = IPSocket::getaddress( spl )
        puts "Server IP Address: " + serverip.to_s
	
elsif domain.start_with? "https://"
	puts " = SSL detected"
	ssl = true
	spl = domain.split("https://")
                spl = spl[1]
          if spl.end_with? ("/")
                spl = spl.split("/")
                spl = spl[0]
                puts "Domain name: " + spl
          end
        serverip = IPSocket::getaddress( spl )
        puts "Server IP Address: " + serverip.to_s

else
	puts " = WTF (no protocol)"
	wtf = true
	  serverip = IPSocket::getaddress( domain )
	  puts "Server IP Address: " + serverip.to_s
end

if ( (http == true || ssl == true)  && domain.end_with?("/") == true ) 
 puts "Full URL, ok..."
 url = domain
elsif ( domain[-1] != "/" &&  (http == true || ssl == true)  )
 puts "Missing trailing slash, adding it..."
 url = domain + "/"
elsif ( domain[-1] != "/" &&  (http != true && ssl != true)  )
 puts "Missing trailing slash and protocol, adding http....Adding the slash..."
 url = "http://" + domain + "/"
elsif ( domain[-1] == "/" && domain[0...4] != "http" )  
 puts "Adding http:// to whatever you put in...Trailing slash there..."
 url = "http://" + domain
end

# cURL routine
c = Curl::Easy.new( url ) do |curl|
  curl.headers["User-Agent"] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A"
  curl.verbose = false
end

puts "===================================="
puts "Testing URL ... " + url

# Run the curl
c.perform

http_return_code = c.response_code
puts "Server response code: "

# Response
case http_return_code
when 100..199
  puts http_return_code.to_s + " Informational"
when 200..299
  puts http_return_code.to_s + " Successfull"
when 300..399
  puts http_return_code.to_s + " Redirect......... Was it http?"
  if url[0...5] == "https" then puts "Protocol used: https. Maybe the www part?"
  else 
	puts "It was http: " + url
	url = "https://" + domain + "/" # bad
	puts " = Trying it again... " + url
	   c.perform
		new_code = c.response_code
		puts " = New server response code: " + new_code.to_s
		if (300..399) === new_code then puts " = = Still 30x. Maybe the www part?"
		end
  end
when 404                      
  puts http_return_code.to_s + " *** Page not found" 
when 400..499                 
  puts http_return_code.to_s + " Client Error" 
when 500..599
  puts http_return_code.to_s + " Server Error"
else
  puts http_return_code.to_s + " ?? Unknown"
end
