# REQ GEMS #######
require 'csv'     #
require 'curb'    #
require 'socket'  #
require 'colorize'#
##################
# a script to display some information about local server

module Admin
TASKS = {update: true, dist: false, clean: true, apf: true}
def flush(path)
 if (File.exists?( path ))
	begin
	file = File.open( path, 'r') # open for reading
	puts "Last modified: #{file.mtime}".white.on_blue
	puts file.read
	puts " --- EOF --- ".blue
	file.close
	rescue Errno::ENOENT => e
  	 puts "Could not open #{ path } because of exception: #{ e.message }".white.on_red
 	end
 end
end
def find_package( name )
 check = system( "dpkg-query -l #{name}")
end
end

include Admin

#### DEF ################
TASKS[:update] = false
TASKS[:dist] = false
hosts = '/etc/hosts'
hname = '/etc/hostname'
#########################

# FILES TO MANUALLY CHECK
puts "Check what all is served from this server: ".white.on_yellow
	system( "uname -api")
	system( "apache2ctl -v" )
	system( "apache2ctl -l" )
puts "Hosts:"
	flush( hosts )
puts "Hostname:"
	flush( hname )
# needs foreach
pack = "apf"
puts "Has #{pack}?"
find_package( pack )

# === #
puts "Checked? y/n ".white.on_yellow
ok = gets
ok.chomp!

if ( ok == "y" )
 
 puts "TASKS: ".white.on_blue
 TASKS.each {|key, value| puts "Will be done #{key}? - #{value}".blue}
 if TASKS[:update]
 apt = system( "apt-get update && apt-get upgrade -y" )
  if apt then puts " - Updated".green end
 end#upd
 if TASKS[:dist]
 dist = system( "apt-get dist-upgrade" )
  if system then puts " - Distribution upgrade".green end
 end#dist
 if TASKS[:clean]
 clean = system( "apt-get autoremove -y && apt-get autoclean" )
  if clean then puts " - Cleaned.".green end
 end#clean
 if TASKS[:apf]
 apf = system( "apf -r")
  if apf then puts " - Firewall restarted".green end
 end#apf
elsif ( ok == "n" )
  puts "abort"

else 
 puts "Wrong input.".red
 # return!
end#command flow


