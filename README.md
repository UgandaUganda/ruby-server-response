## whats in here

### response\_csv
reads a csv file (one url a line), does ping, response code etc

to do:
 
- some 30x handling

### mirror
wgets a website with subpages and all 

### response
reads the domain name/url from stdin

- to rewrite: make it a class

### test/*

- study, tests etc

### data/*

- dummy data

### modules/*

- modules in sep files
