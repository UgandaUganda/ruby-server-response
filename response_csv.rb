# REQ GEMS #######
require 'csv'     #
require 'curb'    #
require 'socket'  #
require 'colorize'#
##################

require "./modules/response.rb"

include Response

# accepted file extensions
accepted = [".csv", ".CSV"]

##############################
puts "Path to your csv file :"
input = $stdin.gets.chomp 
#input = "./data/servers.csv"
path = input.to_s
##############################

puts "Looking up file #{path}..."

if File.file?(path) == true
	puts "File exists"
	base = File.basename( path )
		puts "Basename: " + base
	ext =  File.extname( base )
		puts "Extension: " + ext

	if accepted.include? ( File.extname( path ) )
  		puts "... it's a csv, importing..."
  		CSV.foreach( path, converters: :all ) do |row|	
			#puts row
			row = row.to_s
			row = row.gsub(/[""]/, "")	
			if row[0] == "[" then row[0] = '' end
			if row[-1] == "]" then row[-1] = '' end
			url = row.to_s
			unless row.empty?
			 puts "Checking... #{ url }".underline
			 Response.url_protocol ( url )
			 Response.get_ip ( url )
			 Response.curl( url )
			 
			end
		end
	else 
		puts "File at #{path} is not a csv file, exit.".red
	end
else
	puts "No such file found, exit.".red
end


