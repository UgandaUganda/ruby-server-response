# get mirror of website wget

puts "Domain: "
domain = gets
domain.chomp!

# valid URL
if ( domain[0...4] == "http" && domain[-1] == "/" ) # dumb
 puts "Full URL, ok..."
 url = domain
elsif ( domain[-1] != "/" && domain[0...4] == "http" )
 puts "Missing trailing slash..."
 url = domain + "/"
elsif ( domain[-1] != "/" && domain[0...4] != "http" )
 puts "Missing trailing slash and protocol, adding http...."
 url = "http://" + domain + "/"
elsif ( domain[-1] == "/" && domain[0...4] != "http" )  
 puts "Adding http:// to whatever you put in..."
 url = "http://" + domain
end

# k...
puts "================================================================================================"
puts "Mirroring ... " + url + " into subdir in  ./mirrors"
puts "================================================================================================"

# do...
%x|wget --mirror -p --convert-links -P ./mirrors #{url}|

