<!DOCTYPE html>

<html lang="en-GB">

  <head>

    <meta charset='utf-8'>
    <meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'>

    <title>
        About BelievePerform       ·       The UK&#039;s leading Sports Psychology Website    </title>

    <link rel="pingback" href="http://believeperform.com/xmlrpc.php">

    <meta content='' name='description'>

    <meta name="google-site-verification" content="ctFAfvqv-dhpW4W6oE-alvmk9wECkxIudeJTlVoGWaE" />

    <meta content='True' name='HandheldFriendly'>
    <meta content='320' name='MobileOptimized'>
    <meta id='viewport' name='viewport'>
    <script>
      (function(doc) {
        var viewport = document.getElementById('viewport');
        if ( navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i)) {
          doc.getElementById("viewport").setAttribute("content", "width=device-width, initial-scale=1");
        } else if ( navigator.userAgent.match(/iPad/i) ) {
          doc.getElementById("viewport").setAttribute("content", "width=1280");
        } else {
          doc.getElementById("viewport").setAttribute("content", "width=device-width, initial-scale=0.5");
        }
      }(document));
    </script>
    <meta content='yes' name='apple-mobile-web-app-capable'>
    <meta content='on' http-equiv='cleartype'>
    <meta content='yes' name='apple-mobile-web-app-capable'>
    <meta content='black' name='apple-mobile-web-app-status-bar-style'>
    <meta content='' name='apple-mobile-web-app-title'>

    <link href='favicon.png' rel='shortcut icon'>

    <link href='//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css' rel='stylesheet'>
    <link rel="stylesheet" href="http://believeperform.com/wp-content/themes/sportinmind2015/style.css?ver=1429699569" type="text/css" media="screen, projection" />
    <script src='http://believeperform.com/wp-content/themes/sportinmind2015/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js'></script>

    <link rel="alternate" type="application/rss+xml" title="The UK&#039;s leading Sports Psychology Website &raquo; Feed" href="http://believeperform.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="The UK&#039;s leading Sports Psychology Website &raquo; Comments Feed" href="http://believeperform.com/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="The UK&#039;s leading Sports Psychology Website &raquo; About BelievePerform Comments Feed" href="http://believeperform.com/about/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/believeperform.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.2.6"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='canonical' href='http://believeperform.com/about/' />
<link rel="Shortcut Icon" href="http://believeperform.com/wp-content/themes/sportinmind2015/favicon.png" />

  </head>

  <body class="page page-id-628 page-template-default">

    <div class='navBar'>
      <div class='hamburgerIcon js-side-menu'>
        <div class='ham-1'></div>
        <div class='ham-2'></div>
        <div class='ham-3'></div>
      </div>
      <a class='siteLogo' href='/'>
        <img src='http://believeperform.com/wp-content/themes/sportinmind2015/img/sim-logo-80.png'>
      </a>
    </div>

    <div class='siteNav'>
      <div class='siteNavSearch'>
        <i class='icon-search'></i>
        <form action='/' method='get'>
  <input type='search' name='s' id='search' value='' placeholder='Search'>
</form>      </div>
      <div class='mobileNav'>
        <div class='siteNavLinkGroup siteNavLinkGroup-0'>
          <li id="menu-item-8432" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8432"><a href="http://believeperform.com/category/news/">News</a></li>
<li id="menu-item-8433" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8433"><a href="http://believeperform.com/category/performance/">Performance</a></li>
<li id="menu-item-8430" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8430"><a href="http://believeperform.com/category/coaching/">Coaching</a></li>
<li id="menu-item-8434" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8434"><a href="http://believeperform.com/category/wellbeing/">Well Being</a></li>
<li id="menu-item-8431" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8431"><a href="http://believeperform.com/category/education/">Education</a></li>
<li id="menu-item-8457" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8457"><a href="http://believeperform.com/category/athletescorner/">Athletes Corner</a></li>
        </div>
        <div class='siteNavLinkGroup siteNavLinkGroup-1'>
          <div class='siteNavLinkContainer'>
            <a class='siteNavMainLink' href='/about'>About Us</a>
          </div>
          <div class='siteNavLinkContainer'>
            <a class='siteNavMainLink' href='/meet-the-team'>Meet The Team</a>
          </div>
          <div class='siteNavLinkContainer'>
            <a class='siteNavMainLink' href='/contact'>Contact Us</a>
          </div>
          <!--
          <div class='siteNavLinkContainer'>
            <div class='siteNavMainLink'>Newsletter</div>
            <div class='siteNavSearchContainer'>
              <input placeholder='Email address' type='email'>
            </div>
          </div>
          -->
        </div>
        <div class='siteNavLinkGroup siteNavLinkGroup-2'>
          <a class='siteNavFooterLink' href='/privacy'>Privacy Policy</a>
          <a class='siteNavFooterLink' href='/cookie-policy'>Cookie Policy</a>
        </div>
      </div>
    </div>

    <div class='wrap'>

      <div class='siteMenu'>
        <div class='siteMenuInner site-width'>
          <div class='logoContainer'>
            <a href='/'>
              <img src='http://believeperform.com/wp-content/themes/sportinmind2015/img/sim-logo-500.png'>
              <h1>BelievePerform</h1>
            </a>
          </div>
          <div class='searchContainer'>
            <i class='fa fa-search'></i>
            <form action='/' method='get'>
  <input type='search' name='s' id='search' value='' placeholder='Search'>
</form>          </div>
          <nav>
            <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8432"><a href="http://believeperform.com/category/news/">News</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8433"><a href="http://believeperform.com/category/performance/">Performance</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8430"><a href="http://believeperform.com/category/coaching/">Coaching</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8434"><a href="http://believeperform.com/category/wellbeing/">Well Being</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8431"><a href="http://believeperform.com/category/education/">Education</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8457"><a href="http://believeperform.com/category/athletescorner/">Athletes Corner</a></li>
          </nav>
          <div class='clear'></div>
        </div>
      </div>
<div class='content'>

  <div class='contentInner pageInner site-width'>

    <h1 class="pageHeader">About BelievePerform</h1>

    <div class='articleBody pageBody'>

              <p><a href="http://46.101.40.63/wp-content/uploads/2013/05/TSIM-800-sq-notext.jpg"><img class=" wp-image-8600  alignright" src="http://46.101.40.63/wp-content/uploads/2013/05/TSIM-800-sq-notext-400x400.jpg" alt="" width="250" height="250" /></a>In today&#8217;s sport, the performance of an elite player depends on their physical, technical and tactical characteristics. Psychology is now playing a much larger role in sport and many sports’ clubs are now recruiting top psychologists to help improve their player’s performance.  In over 70 different countries there are top specialist sport psychologists working, all helping to improve performance.</p>
<p>BelievePerform is a website developed to bring together those who have a passion for the area of Sport Psychology as well as those who just wish to enhance their understanding of the field. Through the website you will be able to access up to date sport psychology articles, podcasts and video interviews. We have a team of over 160 writers who contribute to the website on a monthly basis. These include sport psychologists, PhD students, masters students and undergraduates. We are also proud to introduce &#8220;<a title="Athletes Corner" href="http://46.101.40.63/elite-authors/">Athletes Corner</a>&#8221; where we have brought together a host of elite athletes all with a passion for sport psychology. These athletes will bring their knowledge and experience to give a unique insight into the benefits and application of the psychology of performance.</p>
<p>Athletes, coaches, parents, students, teachers and professional psychologists all whom share a common interest in the field of Sport Psychology are welcomed onto the site.</p>
<p><strong>Managing Director</strong></p>
<p>Adam Morris is managing director of BelievePerform. Adam currently holds an undergraduate degree in Psychology with Sport Science and a masters in Sport and Exercise Psychology. Adam is currently undergoing a PhD with supervision from Professor Andy Lane.  Adam developed BelievePerform in April 2015. After 2 years of running a basic sport psychology website Adam believed it was time for a rebrand. BelievePerform was created with a view to target several different audiences including coaches, athletes, students, teachers and sport psychologists.  Not only did Adam want to focus on sporting performance but he also wanted BelievePerform to focus on general performance.  Adam believes that the mind plays a fundamental role within performance. Whether you are an athlete preparing for a big competition or a student studying for an exam, Adam believes that psychology can influence these aspects in a positive way.</p>
<p>To get in touch with Adam please contact him through the following form.</p>
<p><a href="http://believeperform.com/contact/">Contact Adam</a></p>
      
    </div><!-- articleBody -->

  </div><!-- contentInner -->

</div><!-- content -->

    </div><!-- wrap -->
    <footer>
      <div class='footerInner site-width'>
        <div class='columns'>
          <!--
          <div class='column'>
            <div class='columnInner'>
              <h5>Home</h5>
              <a href='category-page.html'>Latest Articles</a>
              <a href='category-page.html'>Most Popular</a>
            </div>
          </div>
          -->
          <div class='column'>
            <div class='columnInner'>
              <h5>Authors</h5>
              <a href='/wp-admin'>Author Area</a>
              <a href='/write-for-us'>Write For Us!</a>
            </div>
          </div>
          <div class='column'>
            <div class='columnInner'>
              <h5>BelievePerform</h5>
              <a href='/about'>About Us</a>
              <a href='/meet-the-team'>Meet The Team</a>
              <a href='/contact'>Contact Us</a>
            </div>
          </div>
        </div>
        <div class='footerGraphic' style='background: url(http://believeperform.com/wp-content/themes/sportinmind2015/img/footer-graphic.png) no-repeat center center'></div>
      </div>
      <div class='footerBottom'>
        <div class='footerBottomInner site-width'>
          <a class='socialLink' href='https://www.facebook.com/BelievePerform'>
            <i class='fa fa-facebook'></i>
          </a>
          <a class='socialLink' href='https://twitter.com/BelievePHQ'>
            <i class='fa fa-twitter'></i>
          </a>
          <a class='socialLink' href='https://www.youtube.com/user/BelievePerform'>
            <i class='fa fa-youtube'></i>
          </a>
          <a class='socialLink' href='/feed'>
            <i class='fa fa-rss'></i>
          </a>
          <!--
          <a class='socialLink' href='#'>
            <i class='fa fa-pinterest'></i>
          </a>
          <a class='socialLink' href='#'>
            <i class='fa fa-google'></i>
          </a>
          -->
          <span class='space-left'>Copyright © 2015 BelievePerform</span>
          <a class='siteLink space-left' href='/privacy'>Privacy Policy</a>
          <!--
          <a class='siteLink' href='static.html'>Terms & Conditions</a>
          -->
          <a class='siteLink' href='/cookie-policy'>Cookie Policy</a>
          <div class='footerLogo'>
            <img src='http://believeperform.com/wp-content/themes/sportinmind2015/img/sim-logo-40.png'>
            <span>BelievePerform</span>
          </div>
        </div>
      </div>
    </footer>
    <script src='//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>
    <script>
      window.jQuery || document.write('<script src="js/vendor/jquery-1.11.1.min.js"><\/script>')
    </script>

    <link href='http://believeperform.com/wp-content/themes/sportinmind2015/js/plugin/owl-carousel/owl.carousel.css' rel='stylesheet'>
    <link href='http://believeperform.com/wp-content/themes/sportinmind2015/js/plugin/owl-carousel/owl.theme.css' rel='stylesheet'>
    <link href='http://believeperform.com/wp-content/themes/sportinmind2015/js/plugin/owl-carousel/owl.transitions.css' rel='stylesheet'>

    <script src='http://believeperform.com/wp-content/themes/sportinmind2015/js/plugin/owl-carousel/owl.carousel.min.js'></script>
    <script src='http://believeperform.com/wp-content/themes/sportinmind2015/js/plugin/fastclick.js'></script>
    <script src='http://believeperform.com/wp-content/themes/sportinmind2015/js/main.js'></script>
    <script>
      var _gaq=[['_setAccount','UA-41340429-1'],['_trackPageview']];
      (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
      g.src='//www.google-analytics.com/ga.js';
      s.parentNode.insertBefore(g,s)}(document,'script'));
    </script>
    <script type='text/javascript'>
/* <![CDATA[ */
var countVars = {"disqusShortname":"thesportinmind"};
/* ]]> */
</script>
<script type='text/javascript' src='http://believeperform.com/wp-content/plugins/disqus-comment-system/media/js/count.js?ver=4.2.6'></script>
  </body>
</html>