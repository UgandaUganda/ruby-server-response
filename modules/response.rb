module Response
  PTC = { http: false, ssl: false, wtf: false }

  flush = Array.new

  def Response.alive? ( domain )
    p = `ping -c 1 -t 10 #{ domain }`
    if p.include?('1 packets') then puts " - Server alive ".green
    else puts " - Server was not found.".red
    end
  end
  def Response.http? (string)
  	uri = URI.parse (string)
  	%w( http ).include?(uri.scheme)
  end
  def Response.ssl? (string)
	uri = URI.parse (string)
	%w( https ).include?(uri.scheme)
  end
  def Response.url_protocol (url)
   if Response.http? (url)
	PTC[:http] = true
   elsif Response.ssl? (url)
	PTC[:ssl] = true
   else
	PTC[:wtf] = true
   end
  end
  def Response.get_ip ( url )
	if PTC[:http]
		spl = url.split("p://")
		spl = spl[1].to_s
	  	if spl.end_with? ("/") 
			spl = spl.split("/")
			domain = spl[0].to_s
	  	else
			domain = spl.to_s
	  	end
		PTC[:http] = false
	elsif PTC[:ssl]
		spl = url.split('s://')
                spl = spl[1].to_s
                if spl.end_with? ("/")
                        spl = spl.split("/")
                        domain = spl[0].to_s
                else
                        domain = spl.to_s
                end
		PTC[:ssl] = false
	elsif PTC[:wtf]
		domain = url.to_s
		PTC[:wtf] = false
	else 
		puts "It's dead Jim"
	end
	  begin
	  puts " - Domain name #{ domain }"
	  Response.alive? ( domain )
	  serverip = IPSocket::getaddress( domain )
	  puts " - Server IP Address: " + serverip.to_s
	  rescue SocketError => se
	  	puts "!!! SocketError : #{se}".white.on_red
	  end
  end
  def Response.curl (url)
        Curl::Easy.perform( url ) do |curl|
  	curl.headers["User-Agent"] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A"
  	curl.verbose = false
	
	 curl.on_complete do |response|
		puts " - Request complete: "
		case response.response_code
			when 100..199
			  puts " -- " + response.response_code.to_s + " Informational"
			when 200..299
			  puts  " -- #{response.response_code.to_s} Successfull".white.on_green
			when 300..399 
			  puts  " -- #{response.response_code.to_s} Redirected ... Was it http?".white.on_yellow
			when 400..499                 
			  puts  " -- " + response.response_code.to_s + " Client Error".white.on_red.blink
			when 500..599
			  puts  " -- " + response.response_code.to_s + " Server Error".white.on_red.blink
			else
			  puts  " -- #{response.response_code.to_s} ?? Something rotten".red
		end#case
	 end#success
	 curl.on_failure do |response|
		puts " - REQUEST FAILED: #{response.response_code}".red
	 end#failure
	 
	end#do
  end#curl
end
